import numpy as np
import pandas as pd

def itemsBolsa(tabla,items,n,w):
    while (n>0 and w>0):
        if (tabla[n,w] != tabla[n-1,w]):
            print("Elemento en mochila: %d" %n)
            n -= 1
            w -= items.iloc[n,1]
        else:
            n -= 1

def mayorGanancia(items,peso):
    tabla = np.zeros([len(items)+1,peso+1], dtype=int)
    for i in range(1,len(items)+1):
        for w in range(peso+1):
            if(items.iloc[i-1,1]<=w):
                if(items.iloc[i-1,2]+tabla[i-1,w-items.iloc[i-1,1]]>tabla[i-1,w]):
                    tabla[i,w] = items.iloc[i-1,2]+tabla[i-1,w-items.iloc[i-1,1]]
                else:
                    tabla[i,w] = tabla[i-1,w]
            else:
                tabla[i,w] = tabla[i-1,w]
    print("Tabla resultante:")
    print(tabla)
    print("Beneficio máximo: $%d" %tabla[i,w])
    itemsBolsa(tabla,items,i,w)

def comprobarPeso(peso):
    if (peso.isdigit()):
        peso = int(peso)
        if (peso>0):
            return True
        else:
            print("Ingrese un valor válido: ")
            return False
    else:
        print ("Ingrese un número entero: ")
        return False

peso = input("Ingrese el peso de la mochila (numero entero): ")
while(comprobarPeso(peso) == False):
    peso = input()
peso = int(peso)
datos = pd.read_csv("items.csv")
print("Peso: %d"%peso)
print("Items:")
print(datos)
mayorGanancia(datos,peso)