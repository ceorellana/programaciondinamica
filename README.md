# Programacion Dinamica #

### Lenguaje utilizado ###

Python 3.6.1  

### Librerias utilizadas ###

* Pandas
* Numpy

### Uso del programa ###

* El archivo mochila0-1 pide al usuario ingresar el peso de la mochila. El programa mostrará la máxima ganancia posible, asi como los itemas necesarios para tener esta ganancia
* En el archivo items.csv se tienen los items que pueden ir dentro de la mochila. Este puede ser modificado como se desee mientras se mantenga el formato establecido (Elemento,Peso,Valor)